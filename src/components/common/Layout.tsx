/* This example requires Tailwind CSS v2.0+ */
import { Fragment, ReactNode } from 'react';
import { Disclosure, Menu, Transition } from '@headlessui/react';
import { Bars3Icon, XCircleIcon } from '@heroicons/react/24/outline';
import Link from 'next/link';
import Head from "next/head";
import { cn } from '@/lib/utils';





const Layout = ({ children }: { children: ReactNode }) => {


    const navigation = [
        { name: 'item', href: '/item' },
        { name: 'item', href: '/item' },
        { name: 'item', href: '/item' },
        { name: 'item', href: '/item' },
        
    ]
    const userNavigation = [
        { name: 'Déconnexion', href: '/api/auth/signout', onClick: () => { }, connected: true },
        { name: 'Connexion', href: '/api/auth/signin', onClick: () => { }, connected: false }
    ]

    return (
        <div className='max-w-full'>
            <Head>
                <title>Beach Volley Challeng</title>
              

            </Head>


            <div className="bg-white">
                <Disclosure as="nav" className="bg-white border-b border-gray-200">
                    {({ open }) => (
                        <>
                            <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
                                <div className="flex justify-between h-16">
                                    <div className="flex">
                                        <div className="">
                                            <Link href="/">
                                                <img
                                                    className="block h-full rounded-full p-2"
                                                    src="./logo.png"
                                                    alt="Mon Autre Chez Moi logo"
                                                />
                                            </Link>
                                        </div>
                                        <div className="hidden sm:-my-px sm:ml-6 sm:flex sm:space-x-8">
                                            {navigation.map((item) => (
                                                <a
                                                    key={item.name}
                                                    href={item.href}
                                                    className={cn(
                                                        item.href === '/'
                                                            ? 'border-blue-500 text-gray-900'
                                                            : 'border-transparent text-gray-500 hover:border-gray-300 hover:text-gray-700',
                                                        'inline-flex items-center px-1 pt-1 border-b-2 text-sm font-medium'
                                                    )}
                                                    aria-current={item.href === '/' ? 'page' : undefined}
                                                >
                                                    {item.name}
                                                </a>
                                            ))}
                                        </div>
                                    </div>
                                    <div className="hidden sm:ml-6 sm:flex sm:items-center">
                                        {/* Profile dropdown */}
                                        {
                                            <Link href='api/auth/signin'>Connexion</Link>
                                        }

   
                                            <Menu as="div" className="ml-3 relative">
                                                {({ open }) => (
                                                    <>
                                                        <div>
                                                            <Menu.Button>
                                                                <div className="flex items-center gap-2">
                                                                    <div>
                                                                       Avatar

                                                                    </div>
                                                        
                                                                </div>
                                                            </Menu.Button>
                                                        </div>
                                                        <Transition
                                                            show={open}
                                                            as={Fragment}
                                                            enter="transition ease-out duration-200"
                                                            enterFrom="transform opacity-0 scale-95"
                                                            enterTo="transform opacity-100 scale-100"
                                                            leave="transition ease-in duration-75"
                                                            leaveFrom="transform opacity-100 scale-100"
                                                            leaveTo="transform opacity-0 scale-95"
                                                        >
                                                            <Menu.Items
                                                                static
                                                                className="origin-top-right absolute right-0 mt-2 w-48 rounded-md shadow-lg py-1 bg-white ring-1 ring-black ring-opacity-5 focus:outline-none"
                                                            >
                                                                {userNavigation.map((item) => (
                                                                    <Menu.Item key={item.name}>
                                                                        {({ active }) => (
                                                                            <a
                                                                                href={item.href}
                                                                                onClick={item?.onClick ? item.onClick : undefined}
                                                                                className={cn(
                                                                                    active ? 'bg-gray-100' : '',
                                                                                    'block px-4 py-2 text-sm text-gray-700'
                                                                                )}
                                                                            >
                                                                                {item.name}
                                                                            </a>
                                                                        )}
                                                                    </Menu.Item>
                                                                ))}

                                                            </Menu.Items>
                                                        </Transition>
                                                    </>
                                                )}
                                            </Menu>
                                        
                                    </div>
                                   
                                     <div className="-mr-2 flex items-center sm:hidden">
                                     {/* Mobile menu button */}
                                     <Disclosure.Button
                                         className="bg-white inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-primary">
                                         <span className="sr-only">Open main menu</span>
                                         {open ? (
                                             <XCircleIcon className="block h-6 w-6" aria-hidden="true" />
                                         ) : (
                                             <Bars3Icon className="block h-6 w-6" aria-hidden="true" />
                                         )}
                                     </Disclosure.Button>
                                 </div>
                 
                                   
                                </div>
                            </div>

                            <Disclosure.Panel className="sm:hidden">
                                <div className="pt-2 pb-3 space-y-1">
                                    {navigation.map((item) => (
                                        <a
                                            key={item.name}
                                            href={item.href}
                                            className={cn(
                                                item.href === '/'
                                                    ? 'bg-blue-50 border-blue-500 text-blue-700'
                                                    : 'border-transparent text-gray-600 hover:bg-gray-50 hover:border-gray-300 hover:text-gray-800',
                                                'block pl-3 pr-4 py-2 border-l-4 text-base font-medium'
                                            )}
                                            aria-current={item.href === '/' ? 'page' : undefined}
                                        >
                                            {item.name}
                                        </a>
                                    ))}
                                </div>
                                <div className="pt-4 pb-3 border-t border-gray-200">
                                   
                                        <div className="flex items-center px-4">
                                            <div className="flex-shrink-0">
                                                
                                            </div>
                                        </div>
                                   

                                    <div className="mt-3 space-y-1">
                                        {userNavigation.map((item) => (
                                            <a
                                                key={item.name}
                                                href={item.href}
                                                className="block px-4 py-2 text-base font-medium text-gray-500 hover:text-gray-800 hover:bg-gray-100"
                                            >
                                                {item.name}
                                            </a>
                                        ))}
                                    </div>
                                </div>
                            </Disclosure.Panel>
                        </>
                    )}
                </Disclosure>

                <div className="py-10">
                    <header>
                        <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
                            <h1 className="text-3xl font-bold leading-tight text-gray-900">{ }</h1>
                        </div>
                    </header>
                    <main>
                        <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
                            {/* Replace with your content */}
                            <div className="px-4 py-8 sm:px-0">
                                {children}
                            </div>
                            {/* /End replace */}
                        </div>
                    </main>
                </div>
            </div>
        </div>
    )
}

export default Layout;

import React from "react";

const PrimaryButton = (props: { className: any; type: "submit" | "button" | "reset" | undefined; children: string | number | boolean | React.ReactElement<any, string | React.JSXElementConstructor<any>> | Iterable<React.ReactNode> | React.ReactPortal | Promise<React.AwaitedReactNode> | null | undefined; }) => {
    const componentClassName = "inline-flex items-center px-2.5 py-1.5 border border-transparent text-xs font-medium " +
        "rounded shadow-sm text-white bg-blue-600 hover:bg-blue-700 focus:outline-none focus:ring-2 " +
        "focus:ring-offset-2 focus:ring-blue-500";
    const propsClassName = props.className;
    const className = [componentClassName, propsClassName].join(' ');

    return <button
        type={props.type ? props.type : 'button' }
        className={className}>
        {props.children}
    </button>;
}

export default PrimaryButton;

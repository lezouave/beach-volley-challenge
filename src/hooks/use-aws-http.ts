import {useContext, useState} from "react";
import {MessageContext} from "../store/message";

const useAwsHttp = () => {
    const [isLoading, setIsLoading] = useState(false);
    const [errors, setErrors] = useState(null);
    const [data, setData] = useState(null);
    const messageCtx = useContext(MessageContext);
    
    const awsSendRequest = async (options: any, transformData: (arg0: any) => void, successMessage: any) => {
        
        setIsLoading(true);
        
       /* try {
            const response = await API.graphql(options);
            transformData(response);
        } catch (error: any) {
            setErrors(error.message);
            (messageCtx as any).setMessageObject({type: 'error', message: error.message});
            setIsLoading(false);
        }
        */
        setIsLoading(false);
        if(successMessage){
            (messageCtx as any).setMessageObject({type: 'success', message: successMessage});
        }
    };
    
    return {
        isLoading,
        errors,
        awsSendRequest
    }
};

export default useAwsHttp;

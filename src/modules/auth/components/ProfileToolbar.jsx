import React from 'react';
import {AmplifySignOut} from '@aws-amplify/ui-react';


export default function ProfileToolbar(){
    const {user} = useUser();

    return (
        <>
        <div>Bonjour <b>{user.nickname}</b></div>

        <AmplifySignOut className=''/>
    </>
    )

}

 


import { createContext, useState } from "react"


const initCtx = {
    locale: 'en',
    pathname: ''
}

export const RouterContext = createContext(initCtx);

const RouterProvider = ({ children }: { children: React.ReactNode }) => {
    const [locale, setLocale] = useState('');
    const [pathname, setPathname] = useState('');

    /*setRouter = (locale, pathname) => {
        setLocale(locale);
        setPathname(pathname);
    }

    */

    const ctx = {
        locale: locale,
        pathname: pathname,
        //setRouter
    }

    return <RouterContext.Provider value={ctx}>{children}</RouterContext.Provider>
}

export default RouterProvider;
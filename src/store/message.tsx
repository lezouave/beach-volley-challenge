import {SetStateAction, createContext, useState} from "react";

const initCtx = {};

export const MessageContext = createContext(initCtx);

import { ReactNode } from "react";

const MessageProvider = ({ children }: { children: ReactNode }) => {
    const [message, setMessage] = useState(null);
    
    function setMessageObject(message: SetStateAction<null>) {
        setMessage(message);
    }
    
    const value = {
        message: message,
        setMessageObject
    }
    
    return <MessageContext.Provider value={value}>{children}</MessageContext.Provider>
};

export default MessageProvider;

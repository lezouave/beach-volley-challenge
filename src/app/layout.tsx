'use client'

import "../../styles/globals.css";
import "react-big-calendar/lib/css/react-big-calendar.css";
import Layout from "../components/common/Layout";
import MessageProvider from "../store/message";
import { SessionProvider } from "next-auth/react";
import UserProvider from "@/store/user";
import GoogleAnalytics from "@/components/GoogleAnalytics";


function RootLayout({ children }: { children: React.ReactNode }) {
  return (
    <html lang="fr">
      <head>
        <meta charSet="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      </head>
      <GoogleAnalytics />
      <body className="w-full">
          <SessionProvider>
            <MessageProvider>
              <UserProvider>
                <Layout>
                  {children}
                </Layout>
              </UserProvider>
            </MessageProvider>
          </SessionProvider>
      </body>
    </html>
  );
}

export default RootLayout;
